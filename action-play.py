#!/usr/bin/env python3

from snipsTools import SnipsConfigParser
from hermes_python.hermes import Hermes

import os
import time

# imported to get type check and IDE completion
from hermes_python.ontology.dialogue.intent import IntentMessage



class PLAY:
    """class used to wrap action code with mqtt connection
       please change the name referring to your application
    """

    def __init__(self):
        # get the configuration if needed

        self.config = None

        # start listening to MQTT
        self.start_blocking()

 
    @staticmethod
    def play_callback(hermes: Hermes,
                          intent_message: IntentMessage):
        
        result_sentence = "Ich habe leider nicht verstanden was du meinst."
        if (len(intent_message.slots) != 0):
            name = intent_message.slots.Songname.first().value # We extract the value from the slot "Farbe"
            if (name == 'pq-Formel'):
                #os.system ('omxplayer' + ' /home/pi/music/pq_formel.mp3')
                #time.sleep(10)
                hermes.publish_end_session(intent_message.session_id, "x ist minus p halbe plus minus die wurzel aus p halbe ins quadrat minus q")
            elif(name == 'abc-Formel'):
                hermes.publish_end_session(intent_message.session_id, "x ist minus b plus minus die wurzel aus b quadrat minus vier a c geteilt durch 2 a")
        else:
            # terminate the session 
            hermes.publish_end_session(intent_message.session_id, result_sentence)

    # register callback function to its intent and start listen to MQTT bus
    def start_blocking(self):
        with Hermes("localhost:1883") as h:
            h.subscribe_intent('FloD:spieleLied', self.play_callback)\
            .loop_forever()


if __name__ == "__main__":
    PLAY()


